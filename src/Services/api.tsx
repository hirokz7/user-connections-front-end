import axios from "axios";

const api = axios.create({
  baseURL: "https://user-connections-back-end.herokuapp.com/",
});

export default api;
