import { Flex, } from '@chakra-ui/layout';
import { Button, useToast, Heading, Image, Text } from '@chakra-ui/react'
import { useParams, useNavigate } from 'react-router-dom';
import api from '../../Services/api'
import note from '../../Assets/img/note.png'
import background from '../../Assets/img/worldConnections.jpg'



const Card = () => {
  const { user_code }: any = useParams();
  const history = useNavigate()
  const toast = useToast();

  const successToast = () => {
    toast({
      description: "Account Activated!",
      duration: 3000,
      position: "top",
      status: "success",
      title: "Login to check our site!",
    });
  };

  const errorToast = () => {
    toast({
      description: "Wrong User!",
      duration: 3000,
      position: "top",
      status: "error",
      title: "Check the code on your e-mail!"
    });
  };


  const submitFunction = (user_code: string) => {
    api
      .post(`activate_account/${user_code}`)
      .then((response) => { console.log(response)
        successToast()
        history('/');
      })
      .catch((err) => {
        console.log(err)
        errorToast()
      });
  };
  
  
  return (
    <Flex
      w="100%"
      h="100vh"
      justifyContent="flex-start"
      alignItems="center"
      flexDirection="column"
      backgroundImage={background}
      bgSize="cover"
      zIndex='1'
    >   
        <Heading 
          fontFamily= "'Montserrat', sans-serif"
          color='darkblue'
          mt='10%'
        >
            Account Cofirmation
        </Heading>
      
      <Image 
        src={note}
        w='820px'
        paddingTop='0px'
        zIndex='-1'
        position='absolute'
      />
        <Heading
         fontFamily="'Poppins', sans-serif"
          paddingTop='5%'
          letterSpacing='2px'
          lineHeight='50px'
          fontSize='20px'
          textAlign='center'
          color='#616060'
        >
          If this is your username? <br/> <Text fontSize='30px' color='darkblue' textDecor='underline'>{user_code.split('@').splice(0,1).join()}</Text> 
        </Heading>
      <Button
        w='200px'
        h='50px'
        mt='3%'
        fontSize='20px'
        fontWeight='bold'
        color='white'
        fontFamily='fantasy'
        borderRadius='10px'
        backgroundColor="#300c96"
        _hover={{
          background:"#c94a4a"
        }}
        onClick={() => {submitFunction(user_code)}}>
          Activate account!
      </Button>
  
    </Flex>
  );
};
export default Card;
