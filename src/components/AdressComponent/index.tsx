import { Flex, Box } from '@chakra-ui/layout';
import { Button,Input, useToast, Heading, Stack, Image, Text } from '@chakra-ui/react'
import background from '../../Assets/img/backGround.png'
import { useState, useEffect } from 'react';
import api from '../../Services/api';
import { useNavigate, Link as ReachLink } from "react-router-dom";



const AdressComponent = () => {

  const [adress, setAdress] = useState({} as any);

  const [ cep, setCep ] = useState('');
  const [ number, setNumber ] = useState('');
  const [ street, setStreet ] = useState('');
  const [ district, setDistrict ] = useState('');
  const [ city, setCity ] = useState('');
  const [ state, setState ] = useState('');
  const [ enable, setEnable ] = useState(true);
  
  const toast = useToast();
  const history = useNavigate();

  useEffect(() => { 
    if(cep && number && street){
      setEnable(false)
    }
  },[cep, number]);

  const addFailToast = () => {
    toast({
      description: "Wrong CEP informed!",
      duration: 5000,
      position: "top",
      status: "error",
      title: "Check fail!",
    });
  };

  const addRequestFailToast = () => {
    toast({
      description: "You need to be logged!",
      duration: 5000,
      position: "top",
      status: "error",
      title: "Submit fail!",
    });
  };

  const addSuccessToast = () => {
    toast({
      description: "Success",
      duration: 5000,
      position: "top",
      status: "success",
      title: "Adress created!",
    });
  };


  const getAdress = (cep: string) => {
      api.get(`cep/${cep}`).then((response) => {
        setStreet(response.data.logradouro)
        setDistrict(response.data.bairro)
        setCity(response.data.localidade)
        setState(response.data.uf)
        setAdress(response.data)
      }).catch((_) =>{
        addFailToast()
      })
  }

  const submitAdress = (cep:string, number:string) =>{
    const token = JSON.parse(localStorage.getItem("@bemol: token") || "null");
    const data = {
      cep: cep,
      number: number
    }
    api.post('adress', data, {
      headers: { Authorization: `Bearer ${token}` },
    }).then((response) => {
        history('/dashboard')
        addSuccessToast()
    }).catch((_) =>{
      addRequestFailToast()
    })
  }

  return (
    <Flex
      w="100%"
      h="100vh"
      justifyContent="center"
      alignItems="center"
      flexDirection="column"
      backgroundImage={background}
      bgSize="cover"
    
    >
    
      <Flex
        justifyContent='center'
        alignItems='center'
        textAlign='center'
        maxW='100%'
        flexDir='column'
        bg='#2c005f'
        p='100px'
        borderRadius='15px'
        boxShadow= '0px 4px 60px rgb(0, 0, 0)'
      >
        <Stack spacing="2">

          <Heading
            paddingTop='6%'
            letterSpacing='2px'
            lineHeight='50px'
            fontSize='30px'
            textAlign='center'
            color='#616060'
            display='flex'
          >
          <Input 
            bg='#fdfdfd'
            variant='outline' 
            placeholder='CEP' 
            onChange={(e) => setCep(e.target.value) } 
            boxShadow= '0px 0px 10px rgb(0, 0, 0)'
            errorBorderColor='red'
          />
          <Button
            bg='#800000'
            color='white'
            _hover={{ bg: "#310000" }}
            ml='5%'
            boxShadow= '0px 0px 10px rgb(0, 0, 0)'
            onClick={() => getAdress(cep) } 
          >
            Check
          </Button>
          </Heading>
          <Heading
            paddingTop='6%'
            letterSpacing='2px'
            lineHeight='50px'
            fontSize='30px'
            textAlign='center'
            color='#616060'
            display='flex'
          >
            <Input 
              bg='white'
              color='black'
              placeholder='Street' 
              defaultValue={street} 
              isReadOnly={true}
              boxShadow= '0px 0px 10px rgb(0, 0, 0)'
            />
            <Input 
              bg='#fdfdfd'
              variant='outline' 
              placeholder='Number' 
              w='120px'
              ml='5%'
              boxShadow= '0px 0px 10px rgb(0, 0, 0)'
              onChange={(e) => setNumber(e.target.value) } 
              errorBorderColor='red'
              
            />
          </Heading>
          <Heading
            paddingTop='6%'
            letterSpacing='2px'
            lineHeight='50px'
            fontSize='30px'
            textAlign='center'
            color='#616060'
            display='flex'
          >
            <Input 
              bg='white'
              color='black'
              placeholder='District' 
              defaultValue={district} 
              isReadOnly={true}
              boxShadow= '0px 0px 10px rgb(0, 0, 0)'
            />
          </Heading>
          <Heading
            paddingTop='6%'
            letterSpacing='2px'
            lineHeight='50px'
            fontSize='30px'
            textAlign='center'
            color='#616060'
            display='flex'
          >
            <Input 
              bg='white'
              color='black'
              placeholder='City' 
              defaultValue={city} 
              isReadOnly={true}
              boxShadow= '0px 0px 10px rgb(0, 0, 0)'
            />
            <Input 
              bg='#fdfdfd'
              variant='outline' 
              placeholder='State' 
              w='120px'
              ml='5%'
              defaultValue={state} 
              isReadOnly={true}
              boxShadow= '0px 0px 10px rgb(0, 0, 0)'
            />
          </Heading>
        <Box/>
        
        <Button
          bg='#00127a'
          color='white'
          _hover={{ bg: "#070024" }}
          boxShadow= '0px 0px 10px rgb(0, 0, 0)'
          isDisabled = {enable}
          onClick={() => submitAdress(cep, number) } 
        >
          Submit
        </Button>
        </Stack>
      </Flex>
    </Flex>
  );
};
export default AdressComponent;
