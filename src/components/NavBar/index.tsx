import {
  Breadcrumb,
  BreadcrumbItem,
  BreadcrumbLink,
  Flex,
  Image,
  Box,
  useDisclosure,
  Modal,
  ModalOverlay,
  ModalContent,
  Text
} from "@chakra-ui/react";
import logo from "../../Assets/img/bemolLogo.png";
import { Link as RouteLink } from "react-router-dom";
import Login from "../../pages/Login"
import Signup from "../../pages/Signup"

const NavBar = () => {
  const { isOpen: isLoginOpen , onOpen: onloginOpen, onClose: onLoginClose } = useDisclosure()
  const { isOpen: isSignupOpen , onOpen: onSignupOpen, onClose: onSignupClose } = useDisclosure()
  

  return (
    <>
      <Breadcrumb>
        <BreadcrumbItem bg="#03001f"  color="white" w="100%">
          <Flex w="100%" justify="space-between" alignItems="center">
            <Box padding="1rem">
              <BreadcrumbItem>
                <RouteLink to="/">
                  <Image src={logo} alt="logo" ml='30px'width="60px" borderRadius='50px'/>
                </RouteLink>
              </BreadcrumbItem>
            </Box>
            <Box padding="1rem">
              <BreadcrumbItem padding="0px 10px"  color="white">
                <BreadcrumbLink 
                    fontFamily= "'Poppins', sans-serif" 
                    bg="#c94a4a" 
                    padding="4px 25px" 
                    borderRadius='5px'
                    _hover={{ bg: "#471919" }}
                    onClick={onSignupOpen}>
                  Sign up
                </BreadcrumbLink>
              </BreadcrumbItem>
              <BreadcrumbItem padding="0px 10px" marginRight='50px' color="white">
                <BreadcrumbLink 
                  fontFamily="'Poppins', sans-serif" 
                  bg="#300c96"  padding="4px 25px" 
                  borderRadius='5px'
                  _hover={{ bg: "#191a47" }} 
                  onClick={onloginOpen}>
                  Login
                </BreadcrumbLink>
              </BreadcrumbItem>
            </Box>
          </Flex>
        </BreadcrumbItem>
      </Breadcrumb>
      <Modal
        isOpen={isLoginOpen}
        onClose={onLoginClose}
      > 
        <ModalOverlay/>   
              <ModalContent autoFocus={true}>
                <Text position='absolute' zIndex='1' left='90%' 
                      fontFamily= "'Montserrat', sans-serif"
                      fontStyle= 'normal'
                      fontWeight= '700'
                      top='10px'
                      _hover={{ color: "#c94a4a" }}
                      onClick={onLoginClose}>
                        close
                </Text>
                <Login/>
              </ModalContent>
      </Modal>
      <Modal
        isOpen={isSignupOpen}
        onClose={onSignupClose}
      > 
        <ModalOverlay/>   
              <ModalContent autoFocus={true}>
                <Text position='absolute' zIndex='1' left='90%' 
                      fontFamily= "'Montserrat', sans-serif"
                      fontStyle= 'normal'
                      fontWeight= '700'
                      top='10px'
                      _hover={{ color: "#c94a4a" }}
                      onClick={onSignupClose}>
                        close
                </Text>
                <Signup/>
              </ModalContent>
      </Modal>
    </>
  );
};

export default NavBar;
