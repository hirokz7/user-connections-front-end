import { Container, Box, Flex } from "@chakra-ui/layout";
import { ReactNode } from "react";
import { useMediaQuery } from "@chakra-ui/react";

interface ContainerDashboardProps {
  children: ReactNode;
}
const ContainerDashboard = ({ children }: ContainerDashboardProps) => {
  const [mobileVersion] = useMediaQuery("(max-width: 500px)");

  return (
    <Container
      padding="0"
      minH="100%"
      minW="100%"
    >
      {mobileVersion ? (
        <Flex
          justifyContent="center"
          flexDirection="column"
        >
          <Box mb="1.75rem">
          </Box>
          <Flex
            minH="100vh"
            justifyContent="center"
            alignItems="center"
            flexDirection="column"
            width="100%"
          >
            {children}
          </Flex>
        </Flex>
      ) : (
        <Box w="100%" h="100%" 
        bgColor="black.transparent800" 
        padding='0'>
          <Flex minH="100%" justifyContent="space-between" alignItems="center" >
            {children}
          </Flex>
          
        </Box>
      )}
    
    </Container>
  );
};
export default ContainerDashboard;
