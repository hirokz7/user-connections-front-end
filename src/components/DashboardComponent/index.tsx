import { Flex, Box } from '@chakra-ui/layout';
import { Heading, Image, Text } from '@chakra-ui/react'
import paths from '../../Assets/img/peakpx.jpg'
import note from '../../Assets/img/note.png'
import comingSoon from '../../Assets/img/comingsoon.png'
import jwtDecode from "jwt-decode";
import { useEffect, useState } from 'react'
import api from '../../Services/api';

const DashboardComponent = () => {

  const [ user, setUser] = useState({} as any)
  const [ adress, setAdress ] = useState([] as any)
  
    useEffect(() => {
      const token = JSON.parse(localStorage.getItem("@bemol: token") || "null");
      const decoded = jwtDecode<any>(token);
      api.get(`user/${decoded.userData.id}`, {
        headers: { Authorization: `Bearer ${token}` },
      }).then((response) => {
          setUser(response.data)
          setAdress(response.data.adresses)
      }).catch((_) =>{
        console.log('Error getting user!')
      }
    )
    },[]);
  

  return (
    <Flex
      w="100%"
      h="100vh"
      justifyContent="center"
      alignItems="center"
      flexDirection="column"
      backgroundImage={paths}
      bgSize="cover"
      zIndex='-2'
    >
      <Image 
        src={note}
        w='860px'
        
        zIndex='-1'
        position='absolute'
      />
      <Flex
        justifyContent='center'
        alignItems='center'
        textAlign='center'
        maxW='500px'
        flexDir='column'
      >
      <Heading mb='10%' fontFamily= "'Montserrat', sans-serif">Welcome {user.username}!</Heading>
      <Text fontFamily="'Poppins', sans-serif" fontWeight='600' > 
        This site is under creative procces, stay tuned!
      </Text>
      <Image src={comingSoon} w='150px'/>
      <Box mt='10%'>
       <Heading fontSize='20px'>This is your first confirmed adress!</Heading>
        {adress && adress.map((item: any) =>{
          return(
            <Flex flexDir='column'>
              <Text fontFamily="'Poppins', sans-serif" >{item.street} : {item.number}</Text>
              <Text fontFamily="'Poppins', sans-serif" >{item.district}</Text>
              <Text fontFamily="'Poppins', sans-serif" >{item.cep}</Text>
              <Text fontFamily="'Poppins', sans-serif" >{item.city} - {item.state}</Text>
            </Flex>
          )
        })}
      </Box>
     
      </Flex>

      
    </Flex>
  );
};
export default DashboardComponent;
