import AdressComponent from '../../components/AdressComponent'
import ContainerDashboard from '../../components/Container'

const Adress = () => {
    return(
        <ContainerDashboard>
            <AdressComponent/>
        </ContainerDashboard>
    )
}

export default Adress