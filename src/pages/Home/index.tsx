import { Container, Flex } from "@chakra-ui/layout";
import NavBar from "../../components/NavBar";
import background from "../../Assets/img/backGround.png"


const Home = () => {
    return(
        <Container
            padding="0"
            minH="100%"
            minW="100%"
            backgroundImage={background}
            bgSize="cover"
            bgPosition='bottom'
        >
        <NavBar/>
        <Flex
            mt='537px'
            w="100%"
            h="100%"
            justifyContent='center'
            bgSize="cover"
        >
        </Flex>
        </Container>
    )
}

export default Home