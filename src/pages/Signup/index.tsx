import { useNavigate, Link as ReachLink } from "react-router-dom";
import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import api from "../../Services/api";
import {
  Box,
  Button,
  FormControl,
  useToast,
  Flex, 
  Stack
} from "@chakra-ui/react";
import { Input } from "../../components/Form/Input";
import { FaUserAlt, FaLock } from "react-icons/fa";
import { MdEmail } from "react-icons/md";
import { FiUser } from "react-icons/fi"


interface IRegister {
  username: string;
  name: string;
  cpf: string;
  email: string;
  password: string;
}

export const Signup = () => {
  const formSchema = yup.object().shape({
    username: yup
      .string()
      .required("Preenchimento obrigatório!")
      .min(8, "No mínimo 8 caracteres!"),
    name: yup
      .string()
      .required("Preenchimento obrigatório!")
      .min(4, "No mínimo 4 caracteres!"),
    cpf: yup
      .string()
      .required("Preenchimento obrigatório!"),
    email: yup
      .string()
      .required("Preenchimento obrigatório!")
      .email("Email inválido!"),
    password: yup
      .string()
      .required("Preenchimento obrigatório!")
      .min(8, "No mínimo 8 caracteres!"),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(formSchema),
  });

  const history = useNavigate();
  const toast = useToast();

  const addSuccessToast = () => {
    toast({
      description: "",
      duration: 5000,
      position: "top",
      status: "success",
      title: "Account created!",
    });
  };

  const addFailToast = () => {
    toast({
      description: "Verify your e-mail",
      duration: 5000,
      position: "top",
      status: "error",
      title: "Fail submit!",
    });
  };

  const submitFunction = (data: IRegister) => {
    api
      .post("user", data)
      .then((response) => {
        console.log(response)
        addSuccessToast();
        history("/");
      })
      .catch((_) => {
        addFailToast();
      });
  };


  return (
    <Box bg='#81818180'>
      <Box 
        borderRadius='10px'
        position= 'absolute'
        width= '500px'
        height= '500px'
        fontFamily= "'Montserrat', sans-serif"
        fontStyle= 'normal'
        fontWeight= '500'
        bg='#7a7bd6'
        >
          <Flex
                 direction="column"
                 justifyContent='center'
                 alignItems='center'
              >
                <Box>
                  <form onSubmit={handleSubmit(submitFunction)}>
                    <FormControl
                      align='center'
                      padding="0.5rem 4rem"
                      mt="10%"
                    >
                      <Stack spacing="2">
                        
                      <Input
                        border='1px solid black'
                        error={errors.username}
                        icon={FiUser}
                        placeholder="Username"
                        {...register("username")}
                      />
                 
                     <Input
                        border='1px solid black'
                        error={errors.name}
                        icon={FaUserAlt}
                        placeholder="Name"
                        {...register("name")}
                      />
                        <Input
                          border='1px solid black'
                          error={errors.cpf}
                          icon={FaLock}
                          placeholder="CPF"
                          {...register("cpf")}
                        />
                      <Input
                        border='1px solid black'
                        error={errors.email}
                        icon={MdEmail}
                        placeholder="Email"
                        type="email"
                        {...register("email")}
                      />
                        <Input
                        border='1px solid black'
                        error={errors.password}
                        icon={FaLock}
                        placeholder="Password"
                        type="password"
                        {...register("password")}
                      />
                    </Stack>
                    <Button
                      bg="#0c0077"
                      color="white"
                      mt="3"
                      padding='0px 100px'
                      type="submit"
                      _hover={{ bg: "#ac0000" }}
                    >
                      Sign Up
                    </Button>
                    </FormControl>
                  </form>
                </Box>
        </Flex>
      </Box>
    </Box>
  );
};

export default Signup;
