import Card from '../../components/Card'
import ContainerDashboard from '../../components/Container'

const Confirmation = () => {
  return (
    <ContainerDashboard>
      <Card />
    </ContainerDashboard>
  );
};

export default Confirmation
