import { useNavigate, Link as ReachLink } from "react-router-dom";
import { useForm } from "react-hook-form";
import * as yup from "yup";
import { yupResolver } from "@hookform/resolvers/yup";
import api from "../../Services/api";
import login from "../../Assets/img/bemolLogo.png"
import {
  Box,
  Button,
  FormControl,
  Text,
  useToast,
  Image,
} from "@chakra-ui/react";

import { Flex, Stack } from "@chakra-ui/layout";
import { FaLock } from "react-icons/fa";
import { MdEmail } from "react-icons/md";

import { Input } from "../../components/Form/Input";


const Login = () => {
  interface ILogin {
    username: string;
    password: string;
  }

  
  const formSchema = yup.object().shape({
    email: yup
      .string()
      .required("Required field!")
      .email("Invalid Email!"),
    password: yup
      .string()
      .required("Required field!")
      .min(8, "Minimum 8 characters!"),
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(formSchema),
  });

  const history = useNavigate();

  const toast = useToast();

  const addSuccessToast = () => {
    toast({
      description: "",
      duration: 5000,
      position: "top",
      status: "success",
      title: "Welcome to Users Connection!",
    });
  };

  const addFailToast = () => {
    toast({
      description: "Wrong Password or E-mail!",
      duration: 5000,
      position: "top",
      status: "error",
      title: "Check if your account is active!",
    });
  };

  const submitFunction = (data: ILogin) => {
    api
      .post("login/", data)
      .then((response) => {
        const { token } = response.data;
        window.localStorage.setItem(
          "@bemol: token",
          JSON.stringify(token)
        );
        addSuccessToast();
        history( "/adress");
      })
      .catch((_) => {
        addFailToast();
      });
  };


  return (
    <Box bg='#81818180'>
      <Box 
        borderRadius='10px'
        position= 'absolute'
        width= '500px'
        height= '500px'
        fontFamily= "'Montserrat', sans-serif"
        fontStyle= 'normal'
        fontWeight= '500'
        bg='#7a7bd6'

        >
          <Flex
                direction="column"
                justifyContent='center'
                alignItems='center'
               
              >
          <Image src={login} borderRadius='100px' w='100px' mt='50px'/>
                <Box>
                  <form onSubmit={handleSubmit(submitFunction)}>
                    <FormControl
                      align='center'
                      padding="0.5rem 4rem"
                      mt=""
                    >
                      <Stack spacing="2">
                        <Text align='left'>Email:</Text>
                        <Input
                           border='1px solid black'
                          error={errors.email}
                          icon={MdEmail}
                          placeholder="Email"
                          type="email"
                          {...register("email")}
                        />
                        <Text align='left'>Password:</Text>
                        <Input
                           border='1px solid black'
                          error={errors.password}
                          icon={FaLock}
                          placeholder="Password"
                          type="password"
                          {...register("password")}
                        />
                      </Stack>
                      <Button
                        align='center'
                        bg="#0c0077"
                        color="white"
                        mt="30px"
                        type="submit"
                        padding='0px 100px'
                        _hover={{ bg: "#ac0000" }}
                      >
                        Sign In
                      </Button>
                    </FormControl>
                  </form>
                </Box>
        </Flex>
      </Box>
    </Box>
  );
};

export default Login;
