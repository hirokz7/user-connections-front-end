import DashboardComponent from '../../components/DashboardComponent'
import ContainerDashboard from '../../components/Container'

const Dashboard = () => {
    return(
        <ContainerDashboard>
            <DashboardComponent/>
        </ContainerDashboard>
    )
}

export default Dashboard