import { ReactNode } from "react";
import { AuthProvider } from "./Auth";
import { UserProvider } from "./User";

interface ProvidersProps {
  children: ReactNode;
}
const Providers = ({ children }: ProvidersProps) => {
  return (
  
        <AuthProvider>
          <UserProvider>{children}</UserProvider>
        </AuthProvider>
  );
};

export default Providers;
