import {
  createContext,
  useContext,
  useState,
  ReactNode
} from "react";
import { Dispatch, SetStateAction } from "react";
interface UserProviderProps {
  children: ReactNode;
}
interface IUser {
  name: string;
  email: string;
  password: string;
  isAdm: boolean;
  id: number;
}
interface UserContextProps {
  userName: string;
  setUserName: Dispatch<SetStateAction<string>>;
  
}
const UserContext = createContext<UserContextProps>({} as UserContextProps);
export const UserProvider = ({ children }: UserProviderProps) => {
  const [userName, setUserName] = useState<string>("");
  const token = JSON.parse(localStorage.getItem("@to-do: token") || "null");
  
  return (
    <UserContext.Provider
      value={{ userName, setUserName }}
    >
      {children}
    </UserContext.Provider>
  );
};
export const useUser = () => useContext(UserContext);
