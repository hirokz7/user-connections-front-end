import { Routes, Route } from 'react-router-dom';
import Adress from '../pages/Adress';
import Confirmation from '../pages/Confirmation';
import Dashboard from '../pages/Dashboard';
import Home from '../pages/Home';


const RoutesPath = () => {
  return (
    <Routes>
      <Route path='/' element={<Home/>} />
      <Route path='/confirmation/:user_code' element={<Confirmation/>} />
      <Route path='/dashboard' element={<Dashboard/>} />
      <Route path='/adress' element={<Adress/>} />
    </Routes>
  );
};

export default RoutesPath;
