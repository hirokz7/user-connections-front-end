# User Connections 

Esse projeto foi criado no intuíto de usufruir das funcionalidades da api de conexões de usuários,
<https://gitlab.com/hirokz7/user-connections-back-end>, é um projeto em que o frontend e backend trabalham em conjunto com a possibilidade de escalonamento.

# Decrição

Para utilizar a API em desenvolvimento, clone o repositório abaixo:

<https://gitlab.com/hirokz7/user-connections-front-end>

Para utilizar a API em produção, segue link do deploy:

<https://user-connections-front-end.vercel.app>

# Installation

Em desenvolvimento, execute o comando abaixo para installar todas as dependências:

`yarn install`

Entre no projeto <https://gitlab.com/hirokz7/user-connections-back-end> e execute
o comando para iniciar o servidor que trabalhará junto com o front-end:

`docker-compose up`

Ainda no projeto do back-end, entre no arquivo docker-compose.yml e identifique ou escolha
a porta que está rodando o back-end:

```json
 app:
    build: .
    command: npm run dev
    ports:
      - 3007:3000
```

De volta ao projeto do front-end entre na pasta src/Services/api.tsx e selecione a porta do localhoast
conforme a sendo utilizado no back-end:

```json
import axios from "axios";

const api = axios.create({
  baseURL: "http://localhost:3007/",
});

export default api;
```

Agora basta configurar o .env com um e-mail válido e com  para que o e-mail de confirmação de conta seja enviado sem 
problemas para o usuário, se for utilizado gmail, é necessário entrar na sua conta na opção de segurança e habilitar a opção de menor segurança para acesso de apps.

Pronto agora seu projeto está configurado para rodar em desenvolvimento, execute o comando abaixo para iniciar o front-end:

`yarn start`


# Como utilizar

Com a idéia de um design intuitívo, o projeto funciona de forma continua sendo uma função necessária para que 
você possa seguir para a próxima etapa:

Faça o Signup do seu usuário, após isso você receberá um e-mail com um link para a ativação de sua conta.

Entre no seu e-mail, clique no link e confirme a ativação da sua conta.
(caso esteja em produção com o deploy e o e-mail não chegue entrar no link 
<https://user-connections-front-end.vercel.app/confirmation/:user_name@> basta apenas substituir o :user_name,
pelo nome do usuário cadastrado e adicionar o @ no final)

Agora você já pode fazer o Login.

Após realizado o Login você será direcionado para a criação de um endereço, onde você colocará seu CEP e 
automaticamente com uma requisição na API: <https://viacep.com.br>, o seu endereço será automáticamente preenchido
sendo necessário apenas inserir o número.

E assim o fron-end chega na sua última página no momento que é o dashboard.
## Technologies

- React
- TypeScript
- Chakra-Ui
- Yup
- Resolvers
- React-router-dom
- Axios
- Jwt-decode
- React-icons
